# Moodle Vue Components
This is a filter [Moodle](https://moodle.org/) plugin that allows for [VueJs](https://vuejs.org/v2/guide/) integration. To install in Moodle: make a zip file of the **moodle-filter_vuecomponents** directory, and drop that into the *Install Plugins* admin section of your Moodle site. Then fill out the plugin's *Admin Settings* and enable the filter in the *Manage filters* section.

## Functionality
A new Vue instance will be mounted on every div that has the class of "vue-components":

```
<div class="vue-components">
  ...
</div>
```
To make it easier for the WYSIWYG editors, any instance of
```
[vue]
  ...
[/vue]
```
will be converted into the former.

Full VueJs functionality will exist within such div's, including any pre-filtering of html and site-wide custom components defined in the *Admin Settings*.

## Admin Settings
As admin, go to: *Home > Site administration > Plugins > Filters > Vue Components Settings*

Options include:

### Use VueJS?
*Checkbox (yes/no)*
This determines whether or not the VueComponents Plugin is active or not.

### Additional Parent Classes
*Textfield (plain text)*
This text is appended to the main 'vue-components' div class list; do not use commas.

### Content Filter JS
*Textarea (js code)*
This is js code that will be run prior to instantiation of the vue instance; the innerHTML of the vue container, `<div class="vue-components">...</div>`, is exposed as the 'content' variable here. This can be used to expose the custom html components as simple macros for WYSIWYG editors. Sample JS could be: `content = content.replace("[mycomponent]","<my-component>")`.

### Custom Components JS
*Textarea (js code)*
This is the core VueJs code that will be used to set global vue components. For each custom component, the JS should follow the form:
`Vue.component('my-component', { ... })`

See the [Vue Component Docs](https://vuejs.org/v2/guide/components.html) for more info on creating components. If using newest JS syntax, we suggest first transpiling the custom component JS using [Babel](https://babeljs.io/) (e.g.) to make it more compatible with older browsers, and then inputting it into the options textbox.

### Custom Components CSS
*Textarea (css code)*
This is any component related css (not scss!) that is required to style your components. Having it here rather than in *Appearance* admin section keeps custom component things contained.

## Vue Instance page data

By default the main Vue instance will pull data from window.vueData, so if you need to add data to the page, first populate the vueData global variable.

For example:
```
<script>
var vueData = { someProperty:5 }
</script>
```
will now allow you to use {{ someProperty }} in the page templates.

## VueComponents Ajax
Included are two Moodle-specific methods to the standard Vue instance:
- $getComponentState(componentName, callback, allUsers)
- $setComponentState(componentName, content, callback)
- $getModData(modType, instanceName, callback, allUsers)

These allow for the reading and writing of any component data to the Moodle database, as well as reading any mod data. Sample use:
```
<div id="ajax-test">
<textarea v-model="message"></textarea>
<p>This is what you typed last: {{message}}</p>
<button @click="saveState">Save State</button>
</div>

<script>
new Vue({
  el: '#ajax-test',
  data: {
    componentName: 'myTestComponent',
    message: ''
  },
  mounted: function() {
    var vm = this;
    window.addEventListener('vueAjaxLoaded', function() {
      vm.loadState()
    }, false);
  },
  methods: {
    loadState: function() {
      var vm = this;
      this.$loadComponentState(this.componentName, function(error, response) {
        if (error) {
          console.log(error)
        } else {
          vm.message = response
          console.log(response)
        }
      })
    },
    saveState: function() {
      this.$saveComponentState(this.componentName, this.message, function(error, response) {
        if (error) {
          console.log(error)
        } else {
          console.log(response)
        }
      })
    }
  }
})
</script>
```

**IMPORTANT:** The componentName string must be unique for the component, per each course. In other words, treat it as a unique identifier in every course - if you have two components with the same name, they will overwrite each other's database records.
