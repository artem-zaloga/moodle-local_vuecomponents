<?php
// This file is part of VueComponents for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


function filter_vuecomponents_before_standard_top_of_body_html()
{
    global $PAGE;
    $config = get_config('filter_vuecomponents');
    $inline_code = '<script type="text/javascript" src="'.$config->vuejs.'"></script>';
    $inline_code .= $config->componentcss ? '<link href="'.$config->componentcss.'" rel="stylesheet">' : '';
    $inline_code .= $config->componentjs ? '<script type="text/javascript" src="'.$config->componentjs.'"></script>' : '';
    $inline_code .= $config->filterjs ? '<script type="text/javascript" src="'.$config->filterjs.'"></script>' : '';
    $inline_code .= <<<EOT
<script>
window.addEventListener('DOMContentLoaded', function(){
  if (document.body.innerHTML.indexOf('vue-components') > -1) {
    var vueDivs = document.getElementsByClassName('vue-components');
    for (var i = 0; i < vueDivs.length; i++) {

      if (!!Vue.prototype.\$filterContent) vueDivs[i].innerHTML = Vue.prototype.\$filterContent(vueDivs[i].innerHTML);
      new Vue({
        el: vueDivs[i],
        data: function() {
          return {
            state: {}
          }
        }
      });
    }
  }
});
</script>
EOT;
  return $inline_code;
}


function filter_vuecomponents_before_footer()
{
    global $PAGE;
    $config = get_config('filter_vuecomponents');

    // might want to add ajaxmethods to window first
    $PAGE->requires->js_amd_inline(
        <<<EOT
require(['filter_vuecomponents/ajaxcalls'], function (ajaxMethods) {
  Vue.prototype.\$getComponentState = ajaxMethods.getComponentState;
  Vue.prototype.\$setComponentState = ajaxMethods.setComponentState;
  Vue.prototype.\$getUserData = ajaxMethods.getUserData;
  Vue.prototype.\$getModData = ajaxMethods.getModData;
  var event = document.createEvent('Event');
  event.initEvent('vueAjaxLoaded', true, true);
  window.dispatchEvent(event);
});
EOT
    );
}
