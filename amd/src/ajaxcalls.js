// Make sure that the callback error checks:
// callback(error, response) {
//   if (error) {
//     console.log(error)
//   } else {
//     // do something
//   }
// }
// Moodle throws errors in the form of an object:
// {backtrace, debuginfo, errorcode, link, message, moreinfourl}

define(['core/ajax'], function(ajax) {
	var ajaxMethods = {
		// get component state
		getComponentState: function(componentName, callback, allUsers) {
			var courseId = parseInt(document.body.className.match(/course-\d+/g)[0].replace(/course-/, '')) || 1
			ajax.call([{
				methodname: 'filter_vuecomponents_get_component_state',
				args: {
					componentname: componentName,
					courseid: courseId,
					allusers: !!allUsers
				},
				done: function(response) {
					callback(false, allUsers ? JSON.parse(response).map(function(el) {
						return JSON.parse(el)
					}) : JSON.parse(response))
				},
				fail: function(error) {
					callback(error)
				}
			}]);
		},
		// set component state
		setComponentState: function(componentName, content, callback) {
			var courseId = parseInt(document.body.className.match(/course-\d+/g)[0].replace(/course-/, '')) || 1
			ajax.call([{
				methodname: 'filter_vuecomponents_set_component_state',
				args: {
					componentname: componentName,
					courseid: courseId,
					content: JSON.stringify(content)
				},
				done: function(response) {
					callback(false, JSON.parse(response))
				},
				fail: function(error) {
					callback(error)
				}
			}]);
		},
		// get user in-course data
		getUserData: function(callback) {
			var courseId = parseInt(document.body.className.match(/course-\d+/g)[0].replace(/course-/, '')) || 1
			ajax.call([{
				methodname: 'filter_vuecomponents_get_user_data',
				args: {
					courseid: courseId
				},
				done: function(response) {
					callback(false, JSON.parse(response))
				},
				fail: function(error) {
					callback(error)
				}
			}]);
		},
		// get activity data
		getModData: function(modType, instanceName, callback, allUsers) {
			var courseId = parseInt(document.body.className.match(/course-\d+/g)[0].replace(/course-/, '')) || 1

			if (modType == 'journal') {
				ajax.call([{
					methodname: 'filter_vuecomponents_get_journal_data',
					args: {
						instancename: instanceName,
						courseid: courseId,
						allusers: !!allUsers
					},
					done: function(response) {
						callback(false, JSON.parse(response))
					},
					fail: function(error) {
						callback(error)
					}
				}]);
			} else if (modType == 'choice') {
				ajax.call([{
					methodname: 'filter_vuecomponents_get_choice_data',
					args: {
						instancename: instanceName,
						courseid: courseId,
						allusers: !!allUsers
					},
					done: function(response) {
						callback(false, JSON.parse(response))
					},
					fail: function(error) {
						callback(error)
					}
				}]);
			} else if (modType == 'feedback') {
				ajax.call([{
					methodname: 'filter_vuecomponents_get_feedback_data',
					args: {
						instancename: instanceName,
						courseid: courseId,
						allusers: !!allUsers
					},
					done: function(response) {
						callback(false, JSON.parse(response))
					},
					fail: function(error) {
						callback(error)
					}
				}]);
			} else console.log("You're not calling the 'getModData' method correctly.")
		}
	};
	return ajaxMethods;
});
