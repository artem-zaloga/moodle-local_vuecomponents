<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    // Url to VueJs
    $settings->add(new admin_setting_configtext(
        'filter_vuecomponents/vuejs',
        'URL to VueJs',
        '',
        'https://moodlemedia.camhx.ca/Moodle11/component-scripts/vue.min.js',
        PARAM_RAW
    ));

    // URL to component js
    $settings->add(new admin_setting_configtext(
        'filter_vuecomponents/componentjs',
        'URL to Custom Components JS',
        '',
        'https://moodlemedia.camhx.ca/Moodle11/component-scripts/vue-components.js',
        PARAM_RAW
    ));

    // URL to component css
    $settings->add(new admin_setting_configtext(
        'filter_vuecomponents/componentcss',
        'URL to Custom Components CSS',
        '',
        'https://moodlemedia.camhx.ca/Moodle11/component-scripts/vue-components.css',
        PARAM_RAW
    ));

    // URL to Vue innerHTML filter JS
    $settings->add(new admin_setting_configtext(
        'filter_vuecomponents/filterjs',
        'URL to Content Filter JS',
        '',
        'https://moodlemedia.camhx.ca/Moodle11/component-scripts/filter.js',
        PARAM_RAW
    ));
}
