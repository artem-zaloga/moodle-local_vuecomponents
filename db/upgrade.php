<?php
// This file is part of VueComponents for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_filter_vuecomponents_upgrade($oldversion)
{
    global $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2019040406) {

    // Define table vuecomponent to be created.
        $table = new xmldb_table('vuecomponents');

        // Adding fields to table vuecomponent.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('componentname', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table vuecomponent.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table vuecomponent.
        $table->add_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));

        // Conditionally launch create table for vuecomponent.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Vuecomponents savepoint reached.
        upgrade_plugin_savepoint(true, 2019040406, 'filter', 'vuecomponents');
    }

    if ($oldversion < 2019040406) {

    // Define table vuecomponent_responses to be created.
        $table = new xmldb_table('vuecomponents_responses');

        // Adding fields to table vuecomponent_responses.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('componentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('content', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table vuecomponent_responses.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('componentid', XMLDB_KEY_FOREIGN, array('componentid'), 'vuecomponents', array('id'));

        // Adding indexes to table vuecomponent_responses.
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));

        // Conditionally launch create table for vuecomponent_responses.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Vuecomponents savepoint reached.
        upgrade_plugin_savepoint(true, 2019040406, 'filter', 'vuecomponents');
    }




    return true;
}
