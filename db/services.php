<?php
// This file is part of VueComponents for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


 $functions = array(
     'filter_vuecomponents_get_component_state' => array(
         'classname' => 'filter_vuecomponents_external',
         'methodname' => 'get_component_state',
         'classpath' => 'filter/vuecomponents/externallib.php',
         'description' => 'Load component data',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     ),
     'filter_vuecomponents_set_component_state' => array(
         'classname' => 'filter_vuecomponents_external',
         'methodname' => 'set_component_state',
         'classpath' => 'filter/vuecomponents/externallib.php',
         'description' => 'Save component data',
         'type' => 'write',
         'ajax' => true,
         'loginrequired' => true,
     ),
     'filter_vuecomponents_get_user_data' => array(
         'classname' => 'filter_vuecomponents_external',
         'methodname' => 'get_user_data',
         'classpath' => 'filter/vuecomponents/externallib.php',
         'description' => 'Get user in-course data.',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     ),
     'filter_vuecomponents_get_journal_data' => array(
         'classname' => 'filter_vuecomponents_external',
         'methodname' => 'get_journal_data',
         'classpath' => 'filter/vuecomponents/externallib.php',
         'description' => 'Get data from a journal activity.',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     ),
     'filter_vuecomponents_get_choice_data' => array(
         'classname' => 'filter_vuecomponents_external',
         'methodname' => 'get_choice_data',
         'classpath' => 'filter/vuecomponents/externallib.php',
         'description' => 'Get data from a choice activity.',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     ),
     'filter_vuecomponents_get_feedback_data' => array(
         'classname' => 'filter_vuecomponents_external',
         'methodname' => 'get_feedback_data',
         'classpath' => 'filter/vuecomponents/externallib.php',
         'description' => 'Get data from a feedback activity.',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     )
 );
