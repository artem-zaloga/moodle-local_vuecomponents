<?php
// This file is part of VueComponents for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 // Listen for course deletions, and then call filter_vuecomponents_observer::course_content_deleted
 $observers = array(
     array(
         'eventname'   => '\core\event\course_content_deleted',
         'callback'    => 'filter_vuecomponents_observer::course_content_deleted'
     )
 );
