<?php
// This file is part of VueComponents for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->libdir . "/externallib.php");

class filter_vuecomponents_external extends external_api
{
    /*  get vue component state */
    // input parameter type check
    public static function get_component_state_parameters()
    {
        return new external_function_parameters(
            [
              'componentname' => new external_value(PARAM_RAW, 'The component name, unique to this course.'),
              'courseid' => new external_value(PARAM_INT, 'The courseid of the course that this component is in.'),
              'allusers' => new external_value(PARAM_BOOL, 'Get info for all users?'),
            ]
        );
    }

    // the actual webservice function
    public static function get_component_state($componentname, $courseid, $allusers)
    {
        global $DB, $USER;

        // prepare SQL select queries and variables
        $instanceSelect = 'courseid = :courseid AND componentname LIKE :componentname';
        $instanceParams = ['courseid' => $courseid, 'componentname' => $componentname];

        // bail if component instance does not exist
        if (!$DB->record_exists_select('vuecomponents', $instanceSelect, $instanceParams)) {
            return json_encode('No component found.');
        }

        // get component instance id
        $componentID = $DB->get_field_select('vuecomponents', 'id', $instanceSelect, $instanceParams);

        // bail if component content does not exist
        if (!$DB->record_exists_select('vuecomponents_responses', 'componentid = :componentid', ['componentid' => $componentID])) {
            return json_encode(null);
        }

        // get component content
        $content =  $allusers
          ? $DB->get_fieldset_select('vuecomponents_responses', 'content', 'componentid = :componentid', ['componentid' => $componentID])
          : $DB->get_field('vuecomponents_responses', 'content', ['userid' => $USER->id, 'componentid' => $componentID]);

        return $allusers ? json_encode($content): $content;
    }

    // output type check; data types can vary, so just json_encode output and always expect a string
    public static function get_component_state_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /*  set vue component state */
    public static function set_component_state_parameters()
    {
        return new external_function_parameters(
            [
                'componentname' => new external_value(PARAM_RAW, 'The component name, unique to this course.'),
                'courseid' => new external_value(PARAM_INT, 'The courseid of the course that this component is in.'),
                'content' => new external_value(PARAM_RAW, 'The JSON.stringify\'d user content for this component.')]
        );
    }

    public static function set_component_state($componentname, $courseid, $content)
    {
        global $DB, $USER;

        // prepare SQL select queries and variables
        $instanceSelect = 'courseid = :courseid AND componentname LIKE :componentname';
        $instanceParams = ['courseid' => $courseid, 'componentname' => $componentname];

        $contentSelect = 'componentid = :componentid AND userid = :userid';
        $contentParams = ['componentid' => null, 'userid' => $USER->id, 'content' => $content];

        // get component instance id from DB record; create record if it doesn't exist
        if ($DB->record_exists_select('vuecomponents', $instanceSelect, $instanceParams)) {
            $contentParams['componentid'] = $DB->get_field_select('vuecomponents', 'id', $instanceSelect, $instanceParams);
        } else {
            $contentParams['componentid'] = $DB->insert_record('vuecomponents', $instanceParams);
        }

        // update/create component content for instance
        if ($DB->record_exists_select('vuecomponents_responses', $contentSelect, $contentParams)) {
            $contentParams['id'] = $DB->get_field_select('vuecomponents_responses', 'id', $contentSelect, $contentParams);
            $DB->update_record('vuecomponents_responses', $contentParams);
            return json_encode('Content updated.');
        } else {
            $DB->insert_record('vuecomponents_responses', $contentParams);
            return json_encode('Content created.');
        }
    }


    public static function set_component_state_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /*  get user in-course state */
    public static function get_user_data_parameters()
    {
        return new external_function_parameters(
            [
                  'courseid' => new external_value(PARAM_INT, 'The courseid of the course that this user is in.')
                ]
        );
    }

    public static function get_user_data($courseid)
    {
        global $USER;

        // get user stuff here
        $content = [
          'firstname' => $USER->firstname,
          'lastname' => $USER->lastname,
          'email' => $USER->email
        ];

        return json_encode($content);
    }

    public static function get_user_data_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /*  get journal state */
    public static function get_journal_data_parameters()
    {
        return new external_function_parameters(
            [
                  'instancename' => new external_value(PARAM_RAW, 'The journal name, unique to this course.'),
                  'courseid' => new external_value(PARAM_INT, 'The courseid of the course that this journal is in.'),
                  'allusers' => new external_value(PARAM_BOOL, 'Get info for all users?'),
                ]
        );
    }

    public static function get_journal_data($instancename, $courseid, $allusers)
    {
        global $DB, $USER;

        // prepare SQL select queries and variables
        $instanceSelect = 'course = :course AND name LIKE :name';
        $instanceParams = ['course' => $courseid, 'name' => $instancename];

        // bail if journal instance does not exist
        if (!$DB->record_exists_select('journal', $instanceSelect, $instanceParams)) {
            return json_encode('No such journal instance found.');
        }

        // get journal instance id
        $journalID = $DB->get_field_select('journal', 'id', $instanceSelect, $instanceParams);

        // bail if journal content does not exist
        if (!$DB->record_exists_select('journal_entries', 'journal = :journal', ['journal' => $journalID])) {
            return json_encode(null);
        }

        // prepare journal content record mapping
        $mapContent = function ($record) {
            return [$record->text, $record->entrycomment];
        };

        // get journal content
        $content = $allusers
          ? array_values(array_map($mapContent, $DB->get_records_select('journal_entries', 'journal = :journal', ['journal' => $journalID])))
          : $mapContent($DB->get_record('journal_entries', ['userid' => $USER->id, 'journal' => $journalID]));

        return json_encode($content);
    }

    public static function get_journal_data_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /* get choice data */
    public static function get_choice_data_parameters()
    {
        return new external_function_parameters(
            [
                'instancename' => new external_value(PARAM_RAW, 'The choice name, unique to this course.'),
                'courseid' => new external_value(PARAM_INT, 'The courseid of the course that this choice is in.'),
                'allusers' => new external_value(PARAM_BOOL, 'Get info for all users?'),
            ]
        );
    }

    public static function get_choice_data($instancename, $courseid, $allusers)
    {
        global $DB, $USER;

        // prepare SQL select queries and variables
        $instanceSelect = 'course = :course AND name LIKE :name';
        $instanceParams = ['course' => $courseid, 'name' => $instancename];

        // bail if choice instance does not exist
        if (!$DB->record_exists_select('choice', $instanceSelect, $instanceParams)) {
            return json_encode('No such choice instance found.');
        }

        // get choice instance id
        $choiceID = $DB->get_field_select('choice', 'id', $instanceSelect, $instanceParams);

        // bail if choice content does not exist
        if (!$DB->record_exists_select('choice_answers', 'choiceid = :choiceid', ['choiceid' => $choiceID])) {
            return json_encode(null);
        }

        // prepare choice instance options mapping
        $optionsRecords = $DB->get_records_select('choice_options', 'choiceid = :choiceid', ['choiceid' => $choiceID]);
        $options = [];
        foreach ($optionsRecords as $r) {
            $options[$r->id] = $r->text;
        }

        // prepare choice content record mapping
        $mapContent = function ($optionID) use ($options) {
            return $options[$optionID];
        };

        // get choice content
        $content = $allusers
              ? array_map($mapContent, $DB->get_fieldset_select('choice_answers', 'optionid', 'choiceid = :choiceid', ['choiceid' => $choiceID]))
              : $mapContent($DB->get_field('choice_answers', 'optionid', ['userid' => $USER->id, 'choiceid' => $choiceID]));

        return json_encode($content);
    }

    public static function get_choice_data_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /* get feedback state */
    public static function get_feedback_data_parameters()
    {
        return new external_function_parameters(
            [
                'instancename' => new external_value(PARAM_RAW, 'The feedback name, unique to this course.'),
                'courseid' => new external_value(PARAM_INT, 'The courseid of the course that this feedback is in.'),
                'allusers' => new external_value(PARAM_BOOL, 'Get info for all users?'),
            ]
        );
    }

    public static function get_feedback_data($instancename, $courseid, $allusers)
    {
        global $DB, $USER;

        // prepare SQL select queries and variables
        $instanceSelect = 'course = :course AND name LIKE :name';
        $instanceParams = ['course' => $courseid, 'name' => $instancename];

        // bail if feedback instance does not exist
        if (!$DB->record_exists_select('feedback', 'course = :course AND name LIKE :name', ['course' => $courseid, 'name' => $instancename])) {
            return json_encode('No such feedback instance found.');
        }

        // get feedback instance id
        $feedbackID = $DB->get_field_select('feedback', 'id', 'course = :course AND name LIKE :name', ['course' => $courseid, 'name' => $instancename]);

        // bail if feedback content does not exist
        if (!$DB->record_exists_select('feedback_completed', 'feedback = :feedback', ['feedback' => $feedbackID])) {
            return json_encode(null);
        }

        // prepare feedback instance options mapping (bit of a headache; need to look at db table entries to make sense of this)
        $optionsRecords = $DB->get_records_select('feedback_item', 'feedback = :feedback', ['feedback' => $feedbackID]);
        $options = [];

        foreach ($optionsRecords as $r) {
            if (strpos($r->typ, 'choice') === false) {
                $options[$r->id] = ($r->typ == 'label' ? 'label'.$r->presentation : $r->typ);
            } else {
                $options[$r->id] = explode('|', str_replace(['r>','c>','>','####'], ['','','',':'], $r->presentation));
            }
        }

         // prepare conversion of feedback content record to [values...]
        $mapContent = function ($completedID) use ($options, $DB) {

            // prepare feedback content values mapping
            $valueRecords = $DB->get_records_select('feedback_value', 'completed = :completed', ['completed' => $completedID]);
            $valuesMap = [];
            foreach ($valueRecords as $r) {
                $valuesMap[$r->item] = $r->value;
            }

            // map values
            $values = [];
            foreach ($options as $key => $val) {
                if (!is_array($val)) {
                    // not choices
                    $value = strpos($val, 'label') !== false ? substr($val, 5) : $valuesMap[$key];
                } elseif (strpos($valuesMap[$key], '|') !== false) {
                    // multiple choice
                    $value = array_map(function ($choice) use ($val) {
                        return $val[(int)$choice-1];
                    }, explode('|', $valuesMap[$key]));
                } else {
                    // single choice
                    $value = $val[(int)$valuesMap[$key]-1];
                }
                array_push($values, $value);
            }

            return $values;
        };

        // map feedback content record to outputFormat
        $content = $allusers
            ? array_values(array_map($mapContent, $DB->get_fieldset_select('feedback_completed', 'id', 'feedback = :feedback', ['feedback' => $feedbackID])))
            : $mapContent($DB->get_field('feedback_completed', 'id', ['userid' => $USER->id, 'feedback' => $feedbackID]));

        return str_replace(['\r','\n'], ['',''], json_encode($content));
    }

    public static function get_feedback_data_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }
}
