<?php
// This file is part of VueComponents for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter plugin "Vue Components" - add VueJs functionality to Moodle
 *
 * @package    filter_vuecomponents
 * @copyright  2019 Artem Zaloga, CAMH
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class filter_vuecomponents extends moodle_text_filter
{
    public function filter($text, array $options = array())
    {
        $text = str_replace(['[vue]','[/vue]'], ['<div class="vue-components">','</div>'], $text);

        return $text;
    }
}
